<?php
  include('connect.php');
  header("Content-type:text/html; charset=UTF-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);

  // $baseurl = '/kosummarket/pages/';
  $baseurl = '/ms_webadmin/pages/';
  $REQUEST_URI = $_SESSION['RE_URI'];
  $MEMBER = $_SESSION['member'][0];
  // Start Function  //
  $sqlu = "SELECT * FROM t_role WHERE role_id IN ({$MEMBER['role_list']})";
  $queryu = DbQuery($sqlu,null);
  $rowu   = json_decode($queryu, true);
  // $rowu   = $rowu['data'];
  // $dataCount   = $rowu['dataCount'];
  $strPage = '';
  foreach ($rowu['data'] as $k => $value) {
    if($k == 0){
      $strPage .= $value['page_list'];
    }else{
      $strPage .= ','.$value['page_list'];
    }
  }
  $arrPage = array_unique(explode(",",$strPage));
  $arrPage = implode(",",$arrPage);

  $sqlp = "SELECT * FROM t_page WHERE page_id IN ($arrPage)";
  $queryp = DbQuery($sqlp,null);
  $rowp   = json_decode($queryp, true);
  $strModule = '';
  $arr = array();
  $strs = '';
  foreach ($rowp['data'] as $k => $value) {
    $arr[$k] = $value['page_path'];
    $strs .= $value['page_path'];
    if($k == 0){
      $strModule .= $value['module_id'];
    }else{
      $strModule .= ','.$value['module_id'];
    }
  }

  $arrModule = array_unique(explode(",",$strModule));
  $arrModule = implode(",",$arrModule);

  $sqlm = "SELECT * FROM t_module WHERE module_id IN ($arrModule)";
  $querym = DbQuery($sqlm,null);
  $rowm   = json_decode($querym, true);
  $strRoot = '';
  $arr = array();
  foreach ($rowm['data'] as $k => $value) {
    if($k == 0){
      $strRoot .= $value['root_id'];
    }else{
      $strRoot .= ','.$value['root_id'];
    }
  }

  $arrRoot = array_unique(explode(",",$strRoot));
  $arrRoot = implode(",",$arrRoot);



  // check Page Role
  $pagess = substr( str_replace($baseurl,"",$REQUEST_URI),0,-1 );
  $pos = strrpos($strs, $pagess);
  // echo $pagess."<br />";
  // echo $REQUEST_URI;
  if ($pos === false) {
    // exit("<script>alert('ไม่มีสิทธิ์ใช้งาน');window.history.back();</script>");
  }
  // End Function  //

  $page_path = substr(str_replace($baseurl,'',$REQUEST_URI) , 0,-1);
  $sqlRoot   = "SELECT * FROM t_root WHERE root_id IN ($arrRoot) ORDER BY root_seq ASC";
  $queryRoot = DbQuery($sqlRoot,null);
  $rowRoot   = json_decode($queryRoot, true);
    foreach ($rowRoot['data'] as $valueRoot) {
      $root_id = $valueRoot['root_id'];
?>
<li class="header"><?=$valueRoot['root_name']?></li>

<?php
  $sqlModule   = "SELECT * FROM t_module WHERE root_id = '$root_id' AND is_active = 'Y' AND module_id IN ($arrModule) ORDER BY module_order ASC, update_date DESC";
  $queryModule = DbQuery($sqlModule,null);
  $rowModule   = json_decode($queryModule, true);

    foreach ($rowModule['data'] as $valueModule) {
      $module_name = $valueModule['module_name'];
      $module_id = $valueModule['module_id'];
      $module_icon = $valueModule['module_icon'];

      $sqlPage   = "SELECT * FROM t_page WHERE page_path = '$page_path' AND page_id IN ($arrPage)";
      $queryPage = DbQuery($sqlPage,null);
      $rowPage   = json_decode($queryPage, true);
      $module_idPage = @$rowPage['data'][0]['module_id'];
?>
<li class="treeview <?=$module_id==$module_idPage?"active menu-open":""?>">
    <a href="#">
      <i class="<?=$module_icon?>"></i>
      <span><?=$module_name?></span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <?php
        $sqlPage   = "SELECT * FROM t_page WHERE module_id = '$module_id' AND is_active = 'Y' AND page_id IN ($arrPage) ORDER BY page_seq ASC, update_date DESC";
        $queryPage = DbQuery($sqlPage,null);
        $rowPage   = json_decode($queryPage, true);
          foreach ($rowPage['data'] as $value) {
      ?>
      <li class="<?=$page_path==$value['page_path']?"active":""?>"><a href="../<?=$value['page_path']?>/"><i class="<?=$value['page_icon'];?>"></i><?=$value['page_name']?></a></li>
      <?php } ?>
    </ul>
</li>
    <?php } ?>
<?php } ?>
