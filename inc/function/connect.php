<?php
session_start();
date_default_timezone_set("Asia/Bangkok");

function check(){
  echo 1;
}

function DbConnect(){
  $serverName = "192.168.20.4";
  $userName = "daijaii_user";
  $userPassword = "Pass$1234";
  $dbName = "JP_ROADSITE_ADMIN";
  try{
  	$conn = new PDO("sqlsrv:server=$serverName ; Database = $dbName", $userName, $userPassword);
  	$conn->setAttribute(PDO::SQLSRV_ATTR_ENCODING, PDO::SQLSRV_ENCODING_UTF8);
      return $conn;
    }
  catch (Exception $e){

  return $e->getMessage();

  }

}

function DbQuery($sql,$ParamData){
  try {
    $obj  = DbConnect();
    $stm = $obj->prepare($sql);
    if($ParamData != null){
      for($x=1; $x<=count($ParamData); $x++)
      {
        $stm->bindParam($x,$ParamData[$x-1]);
      }
    }
    $stm->execute();
    $arr = $stm->errorInfo();
    $id = $obj->lastInsertId();
    $num = 0;
    while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
      $data['data'][] = $row;
      $num++;
    }
    $data['errorInfo'] = $arr;
    $data['dataCount'] = $num;
    $data['id'] = $id;

    if(isset($data)){
      if($num == 0){
        $data['data'] = "";
      }
      $data['status'] = 'success';
      return json_encode($data);
    }else{
      $data['status'] = 'Fail';
      $data['data'] = "";
      return json_encode($data);
    }
  } catch (Exception $e) {
    $data['dataCount'] = 0;
    $data['status'] = 'Fail';
    $data['data'] = "";
    return  json_encode($data);
    $e->getTraceAsString();
  }
}

function DbInsert($sql,$ParamData){
  $obj = DbConnect();
  $sql  = str_replace("now()","GETDATE()",$sql);
  $stm = $obj->prepare($sql);
  try {
    $stm->execute();
    $id = $obj->lastInsertId();
    return json_encode($id);
  } catch (Exception $e) {
    return  json_encode(array());
    $e->getTraceAsString();
  }
}

?>
