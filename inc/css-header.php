<?php
  include('function/connect.php');
?>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="../../dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="../../dist/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="../../dist/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet" href="../../dist/css/jquery-jvectormap.css">
<!-- select2 -->
<link rel="stylesheet" href="../../dist/css/select2.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<!-- dataTables -->
<link rel="stylesheet" href="../../dist/css/dataTables.bootstrap.min.css">
<!-- smoke -->
<link rel="stylesheet" href="../../dist/css/smoke.css">
<!-- smoke -->
<link rel="stylesheet" href="../../dist/css/main.css">
<!-- owl -->
<link rel="stylesheet" href="../../dist/css/owl.carousel.css">
<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


<div class="loadingImg none">
  <img src="../../image/loading.svg" alt="Loading...">
</div>
