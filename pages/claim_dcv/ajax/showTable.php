<?php
  include("../../../inc/function/connect.php");
  include("../../../inc/function/mainFunc.php");
  header("Content-type:text/html; charset=UTF-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);

    $dateStart = $_GET['dateStart'];
    $dateEnd   = $_GET['dateEnd'];
    $type      = 'CLAIM.'.$_GET['type'];

    $sql   = "SELECT * FROM
              [FLASH_EXPRESS].[dbo].[FLEX_REGIS_DECLARE] REG ,
              [FLASH_EXPRESS].[dbo].[FLEX_CLAIM_DECLARE] CLAIM
              WHERE REG.tracking_no = CLAIM.tracking_no
              AND replace(convert(varchar(10), $type, 102),'.','-') 
              BETWEEN '$dateStart' and '$dateEnd'
              AND REG.claim_status != 'C'
              ORDER BY CLAIM.no_claim ASC";
    $query = DbQuery($sql,null);
    $json  = json_decode($query, true);
    $count = $json['dataCount'];
    $row   = $json['data'];

    $dateStart = DateThai($dateStart);
    $dateEnd = DateThai($dateEnd);
    $str = "วันที่ $dateStart";
    if($dateStart != $dateEnd){
      $str = "วันที่ $dateStart ถึง $dateEnd";
    }

  ?>
  <div class="table-responsive">
    <table class="table table-bordered table-striped" id="tableDisplay" style="width:100%">
    <thead>
      <tr class="text-center">
        <td>ลำดับ</td>
        <td>เลข Tracking</td>
        <td>วันที่เกิดเหตุ</td>
        <td>ต้นทาง</td>
        <td>ปลายทาง</td>
        <td>สาขาที่เกิดเหตุ</td>
        <td>พื้นที่ห่างไกล</td>
        <td>น้ำหนัก</td>
        <td>ค่าขนส่ง</td>
        <td>ค่าเบี้ย</td>
      </tr>
    </thead>
    <tbody>
      <?php
        $total_in = 0;
        $total_fs = 0;
        if($count > 0){

          foreach ($row as $key => $value) {
            $total_in += $value['service_flash']/100;
            $total_fs += $value['claim_backward']/100;
      ?>
      <tr class="text-center">
        <td><?=$key+1?></td>
        <td><?=$value['tracking_no']?></td>
        <td><?=DateThai($value['date_claim'])?></td>
        <td><?=$value['province_receive']?></td>
        <td><?=$value['province_deliver']?></td>
        <td><?=$value['branch_clain'];?></td>
        <td><?=$value['remote_area'];?></td>
        <td><?=number_format($value['server_fs_weight'],2);?></td>
        <td><?=$value['claim_backward']/100?></td>
        <td><?=$value['service_flash']/100?></td>
      </tr>
      <?php }} ?>
    </tbody>
    <footer>
      <tr class="text-center">
        <td><strong>รวม DCV</strong></td>
        <td colspan="6"></td>
        <td></td>
        <td><strong><?=number_format($total_fs,2);?></strong></td>
        <td><strong><?=number_format($total_in,2);?></strong></td>
      </tr>
    </footer>
  </table>
  </div>
  <script>
    $(function () {
      $("#tableDisplay").DataTable();
    })
  </script>
