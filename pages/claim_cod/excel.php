<?php

include("../../inc/function/connect.php");
include("../../inc/function/mainFunc.php");
require_once '../../PHPExcel/Classes/PHPExcel.php';

$dataS = $_GET['dataS'];
$dataE = $_GET['dataE'];
$type  = 'CLAIM.'.$_GET['type'];

$sql   = "SELECT *,CLAIM.claim_backward/100 AS claim_backward, REG.service_flash/100 AS service_flash FROM [FLASH_EXPRESS].[dbo].[FLEX_REGIS] REG , [FLASH_EXPRESS].[dbo].[FLEX_CLAIM] CLAIM
          WHERE REG.tracking_no = CLAIM.tracking_no
          AND convert(varchar, $type, 120) BETWEEN '$dataS 00:00:00' and '$dataE 23:59:59'
          AND REG.claim_status != 'C'
          ORDER BY CLAIM.no_claim ASC";
$query = DbQuery($sql,null);
$json  = json_decode($query, true);
$count = $json['dataCount'];
$row   = $json['data'];

$arr = array(
        'เลข Tracking','วันที่เกิดเหตุ',
        'ต้นทาง','ปลายทาง','สาขาที่เกิดเหตุ',
        'พื้นที่ห่างไกล','น้ำหนัก','ค่าขนส่ง','ค่าเบี้ย'
      );
$arrN = array(
        'tracking_no','date_claim',
        'province_receive','province_deliver','branch_clain',
        'remote_area','server_fs_weight','claim_backward','service_flash'
      );


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setCreator("FlashExpress")
 					  ->setLastModifiedBy("FlashExpress")
 					  ->setTitle("Office 2007 XLSX Test Document")
 					  ->setSubject("Office 2007 XLSX Test Document")
 					  ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
 					  ->setKeywords("office 2007 openxml php")
 					  ->setCategory("Test result file");

  $setCharExcel = setCharExcel(sizeof($arr));
  for ($i=1; $i <=1 ; $i++) {
    for ($j=1; $j <=sizeof($setCharExcel) ; $j++) {

      $objPHPExcel->getActiveSheet()->getColumnDimension($setCharExcel[$j-1])->setAutoSize(true);
      $objPHPExcel->getActiveSheet()->getStyle($setCharExcel[$j-1].$i.':'.$setCharExcel[$j-1].$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
      $objPHPExcel->getActiveSheet()->getStyle( $setCharExcel[$j-1].$i )->getFont()->setBold( true );

      $objPHPExcel->getActiveSheet()->setCellValue($setCharExcel[$j-1].$i,$arr[$j-1]);

    }
  }

  for ($i=2; $i <=sizeof($row)+1 ; $i++) {
    for ($j=1; $j <=sizeof($setCharExcel) ; $j++) {
      $objPHPExcel->getActiveSheet()->getColumnDimension($setCharExcel[$j-1])->setAutoSize(false);
    	$objPHPExcel->getActiveSheet()->setCellValue($setCharExcel[$j-1].$i,$row[$i-2][$arrN[$j-1]]);
    }
  }
  // Rename sheet
  $objPHPExcel->getActiveSheet()->setTitle('COD');
  $objPHPExcel->setActiveSheetIndex(0);


$name = "COD_CLAIM_".date('Ymd');
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$name.'.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

?>
