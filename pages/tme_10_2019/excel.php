<?php

include("../../inc/function/connect.php");
include("../../inc/function/mainFunc.php");
require_once '../../PHPExcel/Classes/PHPExcel.php';


$sql   = "SELECT * FROM [JP_MOBILE_EXPO].[dbo].[REGISTER] ORDER BY id DESC";
$query = DbQuery($sql,null);
$json  = json_decode($query, true);
$count = $json['dataCount'];
$row   = $json['data'];

$arr = array(
        'ชื่อจริง','นามสกุล','เพศ',
        'เบอร์โทรศัพท์มือถือ','วันเกิด',
        'อีเมล์','ระดับการศึกษา','ตำแหน่งงาน','สถานภาพ','รถยนต์',
        'QA1','QA2','QA3','QA4','QA5','วันที่ลงทะเบียน'
      );
$arrN = array(
        'first_name','last_name','sex',
        'mobile','birthdate','email',
        'education','job','status','car',
        'qa1','qa2','qa3','qa4','qa5','date_create'
      );
$name = "TME102019_REPORT_".date('Ymd');

$setCharExcel = setCharExcel(sizeof($arr));

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setCreator("FlashExpress")
 					  ->setLastModifiedBy("FlashExpress")
 					  ->setTitle("Office 2007 XLSX Test Document")
 					  ->setSubject("Office 2007 XLSX Test Document")
 					  ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
 					  ->setKeywords("office 2007 openxml php")
 					  ->setCategory("Test result file");

  for ($i=1; $i <=1 ; $i++) {
    for ($j=1; $j <=sizeof($setCharExcel) ; $j++) {

      $objPHPExcel->getActiveSheet()->getColumnDimension($setCharExcel[$j-1])->setAutoSize(true);
      $objPHPExcel->getActiveSheet()->getStyle($setCharExcel[$j-1].$i.':'.$setCharExcel[$j-1].$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
      $objPHPExcel->getActiveSheet()->getStyle( $setCharExcel[$j-1].$i )->getFont()->setBold( true );

      $objPHPExcel->getActiveSheet()->setCellValue($setCharExcel[$j-1].$i,$arr[$j-1]);

    }
  }

  for ($i=2; $i <=sizeof($row)+1 ; $i++) {
    for ($j=1; $j <=sizeof($setCharExcel) ; $j++) {
      $objPHPExcel->getActiveSheet()->getColumnDimension($setCharExcel[$j-1])->setAutoSize(false);
    	$objPHPExcel->getActiveSheet()->setCellValue($setCharExcel[$j-1].$i,$row[$i-2][$arrN[$j-1]]);
    }
  }
  // Rename sheet
  $objPHPExcel->getActiveSheet()->setTitle('TME102019');
  $objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$name.'.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

?>
