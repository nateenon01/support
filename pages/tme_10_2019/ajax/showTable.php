<?php
  include("../../../inc/function/connect.php");
  header("Content-type:text/html; charset=UTF-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);

  ?>
  <div class="text-right" style="margin-bottom:20px;">
    <button type="button" onclick="exportExcel()" class="btn btn-default btn-flat">Export Excel</button>
  </div>
  <div class="table-responsive">
    <table class="table table-bordered table-striped" width="100%" id="tableDisplay">
    <thead>
      <tr class="text-center">
        <td>No</td>
        <td>ชื่อจริง นามสกุล</td>
        <td>เพศ</td>
        <td>เบอร์โทรศัพท์มือถือ</td>
        <td>วันเกิด</td>
        <td>อีเมล์</td>
        <td>ระดับการศึกษา</td>
        <td>ตำแหน่งงาน</td>
        <td>สถานภาพ</td>
        <td>รถยนต์</td>
        <td>QA1</td>
        <td>QA2</td>
        <td>QA3</td>
        <td>QA4</td>
        <td>QA5</td>
        <td>วันที่ลงทะเบียน</td>
      </tr>
    </thead>
    <tbody>
      <?php
        $sql   = "SELECT * FROM [JP_MOBILE_EXPO].[dbo].[REGISTER] ORDER BY id DESC";
        $query = DbQuery($sql,null);
        $json  = json_decode($query, true);
        $count = $json['dataCount'];
        $row   = $json['data'];
        if($count>0){
        foreach ($row as $key => $value) {
      ?>
      <tr class="text-center">
        <td><?=$key+1?></td>
        <td><?=$value['first_name'].' '.$value['last_name']?></td>
        <td><?=$value['sex']?></td>
        <td><?=$value['mobile']?></td>
        <td><?=$value['birthdate']?></td>
        <td><?=$value['email']?></td>
        <td><?=$value['education']?></td>
        <td><?=$value['job']?></td>
        <td><?=$value['status']?></td>
        <td><?=$value['car']?></td>
        <td><?=$value['qa1']?></td>
        <td><?=$value['qa2']?></td>
        <td><?=$value['qa3']?></td>
        <td><?=$value['qa4']?></td>
        <td><?=$value['qa5']?></td>
        <td><?=$value['date_create']?></td>
      </tr>
      <?php }} ?>
    </tbody>
  </table>
  </div>
  <script>
    $(function () {
      $("#tableDisplay").DataTable();
    });

    function exportExcel(){
      window.location='excel.php';
    }
  </script>
