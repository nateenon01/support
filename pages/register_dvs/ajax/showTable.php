<?php
  include("../../../inc/function/connect.php");
  include("../../../inc/function/mainFunc.php");
  header("Content-type:text/html; charset=UTF-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);

    $dateStart = $_GET['dateStart'];
    $dateEnd   = $_GET['dateEnd'];
    $type      = $_GET['type'];

    $sql   = "SELECT * FROM [FLASH_EXPRESS].[dbo].[FLEX_REGIS_DECLARE]
    WHERE replace(convert(varchar(10), $type, 102),'.','-') BETWEEN '$dateStart' and '$dateEnd'
    AND claim_status != 'C' ORDER BY no_regis ASC";

    $query = DbQuery($sql,null);
    $json  = json_decode($query, true);
    $count = $json['dataCount'];
    $row   = $json['data'];

    $dateStart = DateThai($dateStart);
    $dateEnd = DateThai($dateEnd);
    $str = "วันที่ $dateStart";
    if($dateStart != $dateEnd){
      $str = "วันที่ $dateStart ถึง $dateEnd";
    }

  ?>
  <div class="text-right" style="margin-bottom:20px;">
    <button type="button" onclick="exportExcel()" class="btn btn-default btn-flat">Export Excel</button>
  </div>
  <div class="table-responsive">
    <table class="table table-bordered table-striped" id="tableDisplay" style="width:100%;">
      <thead>
        <tr class="text-center">
          <td>ลำดับ</td>
          <td>เลข Tracking</td>
          <td>วันที่รับประกัน</td>
          <td>จังหวัดต้นทาง</td>
          <td>จังหวัดปลายทาง</td>
          <td>พื้นที่ห่างไกล</td>
          <td>น้ำหนัก</td>
          <td>มูลค่าสิ่งของรับประกัน</td>
          <td>ค่าเบี้ย</td>
        </tr>
      </thead>
      <tbody>
        <?php
          $total_in = 0;
          $total_fs = 0;
          if($count > 0){

            foreach ($row as $key => $value) {
              $total_in += $value['service_dvs']/100;
              $total_fs += $value['price_declare']/100;
        ?>
        <tr class="text-center">
          <td><?=$key+1?></td>
          <td><?=$value['tracking_no']?></td>
          <td><?=DateThai($value['date_receive_product'])?></td>
          <td><?=$value['province_receive']?></td>
          <td><?=$value['province_deliver']?></td>
          <td><?=$value['remote_area'];?></td>
          <td><?=number_format($value['server_fs_weight'],2);?></td>
          <td><?=$value['price_declare']/100?></td>
          <td><?=$value['service_dvs']/100?></td>
        </tr>
        <?php }} ?>
      </tbody>
      <footer>
        <tr class="text-center">
          <td><strong>รวม DCV</strong></td>
          <td colspan="5"></td>
          <td></td>
          <td><strong><?=number_format($total_fs,2);?></strong></td>
          <td><strong><?=number_format($total_in,2);?></strong></td>
        </tr>
      </footer>
    </table>
  </div>
  <script>
    $(function () {
      $("#tableDisplay").DataTable();
    })
  </script>
