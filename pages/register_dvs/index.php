<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>AdminLTE 2 | Dashboard</title>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/register_dvs.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Dashboard
              <small>Version 2.0</small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Register_dvs</a></li>
              <li class="active">Dashboard</li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php include("../../inc/boxes.php"); ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-12">

              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">register_dvs Controller</h3>

                  <div class="box-tools pull-right">
                    <!-- <button onclick="showForm('ADD')" class="btn btn-success btn-flat pull-right">ADD register_dvs</button> -->
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                  <div class="row" style="margin-bottom:20px;">
                    <div class="col-md-8 col-md-offset-2">
                      <div class="form-inline">
                        <div class="form-group">
                          <label>ค้นหา วันที่แจ้งประกัน</label>
                          <input type="date" id="dateStart" class="form-control" value="<?=date('Y-m-d')?>">
                        </div>
                        <div class="form-group">
                          <label> - </label>
                          <input type="date" id="dateEnd" class="form-control" value="<?=date('Y-m-d')?>">
                        </div>
                        <div class="form-group">
                          <select id="type" class="form-control">
                            <option value="create_date" selected>วันที่รับแจ้ง JP</option>
                            <option value="date_receive_product">วันที่รับแจ้งจริง</option>
                          </select>
                        </div>
                        <button type="button" onclick="getTable()" class="btn btn-primary btn-flat">Search</button>
                      </div>
                    </div>
                  </div>

                  <div id="showTable"></div>
                </div>
              </div>
              <!-- /.box -->

              <!-- Modal -->
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">register_dvs Module</h4>
                    </div>
                    <form id="formAddModule" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data">
                      <div id="show-form"></div>
                    </form>
                  </div>
                </div>
              </div>


              </div>
            </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/register_dvs.js"></script>
    </body>
  </html>
