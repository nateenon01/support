<style media="screen">
  .mar-bottom-20{
    margin-bottom: 40px;
  }
</style>
<?php
  include("../../../inc/function/connect.php");
  header("Content-type:text/html; charset=UTF-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);

  $dateStart = $_GET['dateStart'];
  $dateEnd   = $_GET['dateEnd'];


?>

<div class="row">
  <div class="col-md-6 mar-bottom-20">
    <table class="table table-bordered table-striped" style="width:100%;">
      <thead>
        <tr class="text-center">
          <td colspan="4"><strong>REGISER COD</strong></td>
        </tr>
        <tr class="text-center">
          <td><strong>วันที่</strong></td>
          <td><strong>ทุนประกันภัย</strong></td>
          <td><strong>จำนวนกรมธรรม์</strong></td>
          <td><strong>ค่าเบี้ยประกัน</strong></td>
        </tr>
      </thead>
      <tbody>
        <?php
        $sql = "
                select convert(varchar(10), create_date, 103) as create_date,
                isnull(count(tracking_no),0) as counting,
                isnull(sum(service_flash),0) / 100 as service_flash,
                isnull(sum(case remote_area when 'Y' then (service_fs/100)-50 else (service_fs/100) end),0) as ddd
                from [FLASH_EXPRESS].[dbo].[FLEX_REGIS]
                where replace(convert(varchar(10), create_date, 102),'.','-') BETWEEN '$dateStart' and '$dateEnd'
                and claim_status != 'C'
                group by convert(varchar(10), create_date, 103) order by create_date asc
               ";
        $query = DbQuery($sql,null);
        $json  = json_decode($query, true);
        $count = $json['dataCount'];
        $row   = $json['data'];

        $service_flash = 0;
        $counting = 0;
        $ddd = 0;
        if($count > 0){
          foreach ($row as $value) {
            $service_flash += $value['service_flash'];
            $counting      += $value['counting'];
            $ddd += $value['ddd'];
        ?>
        <tr class="text-center">
          <td><?=$value['create_date']?></td>
          <td><?=number_format($value['ddd'],2)?></td>
          <td><?=number_format($value['counting'])?></td>
          <td><?=number_format($value['service_flash'],2)?></td>
        </tr>
        <?php } } ?>
      </tbody>
      <tfoot>
        <tr class="text-center">
          <td><strong>รวม</strong></td>
          <td><strong><?=number_format($ddd,2);?></strong></td>
          <td><strong><?=number_format($counting)?></strong></td>
          <td><strong><?=number_format($service_flash,2);?></strong></td>
        </tr>
      </tfoot>
    </table>
  </div>
  <div class="col-md-6 mar-bottom-20">
    <table class="table table-bordered table-striped" style="width:100%;">
      <thead>
        <tr class="text-center">
          <td colspan="4"><strong>CLAIM COD</strong></td>
        </tr>
        <tr class="text-center">
          <td><strong>วันที่แจ้งเคลม</strong></td>
          <td><strong>ค่าเบี้ยประกัน</strong></td>
          <td><strong>จำนวนเคลม</strong></td>
          <td><strong>สินไหมจ่าย</strong></td>
        </tr>
      </thead>
      <tbody>
        <?php
        $sql = "
                  select convert(varchar(10), b.create_date, 103) as create_date,
                  isnull(count(a.tracking_no),0) as counting,
                  isnull(sum(a.service_flash),0) / 100 as service_flash,
                  isnull(sum(case a.remote_area when 'Y' then (b.claim_backward/100)-50 else (b.claim_backward/100) end),0) as ddd
                  from [FLASH_EXPRESS].[dbo].[FLEX_REGIS] a Inner Join
                  [FLASH_EXPRESS].[dbo].[FLEX_CLAIM] b on a.tracking_no = b.tracking_no
                  where replace(convert(varchar(10), b.create_date, 102),'.','-') BETWEEN '$dateStart' and '$dateEnd'
                  and a.claim_status != 'C'
                  group by convert(varchar(10), b.create_date, 103) order by create_date asc
               ";
        $query = DbQuery($sql,null);
        $json  = json_decode($query, true);
        $count = $json['dataCount'];
        $row   = $json['data'];

        $counting = 0;
        $service_flash = 0;
        $ddd = 0;
        if($count > 0){
          foreach ($row as $value) {
            $service_flash += $value['service_flash'];
            $counting += $value['counting'];
            $ddd += $value['ddd'];
        ?>
        <tr class="text-center">
          <td><?=$value['create_date']?></td>
          <td><?=number_format($value['service_flash'],2)?></td>
          <td><?=number_format($value['counting'])?></td>
          <td><?=number_format($value['ddd'],2)?></td>
        </tr>
        <?php } } ?>
      </tbody>
      <tfoot>
        <tr class="text-center">
          <td><strong>รวม</strong></td>
          <td><strong><?=number_format($service_flash,2);?></strong></td>
          <td><strong><?=number_format($counting)?></strong></td>
          <td><strong><?=number_format($ddd,2);?></strong></td>
        </tr>
      </tfoot>
    </table>
  </div>
  <div class="clearfix"></div>
  <div class="col-md-6 mar-bottom-20">
    <table class="table table-bordered table-striped" style="width:100%;">
      <thead>
        <tr class="text-center">
          <td colspan="4"><strong>REGISER COD บัญชี</strong></td>
        </tr>
        <tr class="text-center">
          <td><strong>วันที่</strong></td>
          <td><strong>ทุนประกันภัย</strong></td>
          <td><strong>จำนวนกรมธรรม์</strong></td>
          <td><strong>ค่าเบี้ยประกัน</strong></td>
        </tr>
      </thead>
      <tbody>
        <?php
        $sql = "
                  select convert(varchar(10), date_receive_product, 103) as create_date,
                  isnull(count(tracking_no),0) as counting,
                  isnull(sum(service_flash),0) / 100 as service_flash,
                  isnull(sum(case remote_area when 'Y' then (service_fs/100)-50 else (service_fs/100) end),0) as ddd
                  from [FLASH_EXPRESS].[dbo].[FLEX_REGIS]
                  where replace(convert(varchar(10), date_receive_product, 102),'.','-') BETWEEN '$dateStart' and '$dateEnd'
                  and claim_status != 'C'
                  group by convert(varchar(10), date_receive_product, 103) order by create_date asc
               ";
        $query = DbQuery($sql,null);
        $json  = json_decode($query, true);
        $count = $json['dataCount'];
        $row   = $json['data'];

        $service_flash = 0;
        $counting = 0;
        $ddd = 0;
        if($count > 0){
          foreach ($row as $value) {
            $service_flash += $value['service_flash'];
            $counting      += $value['counting'];
            $ddd += $value['ddd'];
        ?>
        <tr class="text-center">
          <td><?=$value['create_date']?></td>
          <td><?=number_format($value['ddd'],2)?></td>
          <td><?=number_format($value['counting'])?></td>
          <td><?=number_format($value['service_flash'],2)?></td>
        </tr>
        <?php } } ?>
      </tbody>
      <tfoot>
        <tr class="text-center">
          <td><strong>รวม</strong></td>
          <td><strong><?=number_format($ddd,2);?></strong></td>
          <td><strong><?=number_format($counting)?></strong></td>
          <td><strong><?=number_format($service_flash,2);?></strong></td>
        </tr>
      </tfoot>
    </table>
  </div>
  <div class="col-md-6 mar-bottom-20">
    <table class="table table-bordered table-striped" style="width:100%;">
      <thead>
        <tr class="text-center">
          <td colspan="4"><strong>CLAIM COD บัญชี</strong></td>
        </tr>
        <tr class="text-center">
          <td><strong>วันที่แจ้งเคลม</strong></td>
          <td><strong>ค่าเบี้ยประกัน</strong></td>
          <td><strong>จำนวนเคลม</strong></td>
          <td><strong>สินไหมจ่าย</strong></td>
        </tr>
      </thead>
      <tbody>
        <?php
        $sql = "
                  select convert(varchar(10), b.date_claim, 103) as create_date,
                  isnull(count(a.tracking_no),0) as counting,
                  isnull(sum(a.service_flash),0) / 100 as service_flash,
                  isnull(sum(case a.remote_area when 'Y' then (b.claim_backward/100)-50 else (b.claim_backward/100) end),0) as ddd
                  from [FLASH_EXPRESS].[dbo].[FLEX_REGIS] a Inner Join
                  [FLASH_EXPRESS].[dbo].[FLEX_CLAIM] b on a.tracking_no = b.tracking_no
                  where replace(convert(varchar(10), b.date_claim, 102),'.','-') BETWEEN '$dateStart' and '$dateEnd'
                  and a.claim_status != 'C'
                  group by convert(varchar(10), b.date_claim, 103) order by create_date asc
               ";
        $query = DbQuery($sql,null);
        $json  = json_decode($query, true);
        $count = $json['dataCount'];
        $row   = $json['data'];

        $query = DbQuery($sql,null);
        $json  = json_decode($query, true);
        $count = $json['dataCount'];
        $row   = $json['data'];

        $counting = 0;
        $service_flash = 0;
        $ddd = 0;
        if($count > 0){
          foreach ($row as $value) {
            $service_flash += $value['service_flash'];
            $counting += $value['counting'];
            $ddd += $value['ddd'];
        ?>
        <tr class="text-center">
          <td><?=$value['create_date']?></td>
          <td><?=number_format($value['service_flash'],2)?></td>
          <td><?=number_format($value['counting'])?></td>
          <td><?=number_format($value['ddd'],2)?></td>
        </tr>
        <?php } } ?>
      </tbody>
      <tfoot>
        <tr class="text-center">
          <td><strong>รวม</strong></td>
          <td><strong><?=number_format($service_flash,2);?></strong></td>
          <td><strong><?=number_format($counting)?></strong></td>
          <td><strong><?=number_format($ddd,2);?></strong></td>
        </tr>
      </tfoot>
    </table>
  </div>
  <div class="clearfix"></div>
</div>
<div class="row">
  <div class="col-md-6 mar-bottom-20">
    <table class="table table-bordered table-striped" style="width:100%;">
      <thead>
        <tr class="text-center">
          <td colspan="4"><strong>REGISER DECARE VALUE</strong></td>
        </tr>
        <tr class="text-center">
          <td><strong>วันที่</strong></td>
          <td><strong>มูลค่าสิ่งของรับประกัน</strong></td>
          <td><strong>จำนวนกรมธรรม์</strong></td>
          <td><strong>ค่าเบี้ยประกัน</strong></td>
        </tr>
      </thead>
      <tbody>
        <?php
        $sql = "
                select convert(varchar(10), create_date, 103) as create_date,
                isnull(count(tracking_no),0) as counting,
                isnull(sum(price_declare),0) / 100 as price_declare,
                isnull(sum(case discount when 0 then (service_dvs/100) else (service_dvs_discount/100) end),0) as ddd
                from [FLASH_EXPRESS].[dbo].[FLEX_REGIS_DECLARE]
                where replace(convert(varchar(10), create_date, 102),'.','-') BETWEEN '$dateStart' and '$dateEnd'
                and claim_status != 'C'
                group by convert(varchar(10), create_date, 103) order by create_date asc
               ";
        $query = DbQuery($sql,null);
        $json  = json_decode($query, true);
        $count = $json['dataCount'];
        $row   = $json['data'];

        $price_declare = 0;
        $counting = 0;
        $ddd = 0;
        if($count > 0){
          foreach ($row as $value) {
            $price_declare += $value['price_declare'];
            $counting      += $value['counting'];
            $ddd += $value['ddd'];
        ?>
        <tr class="text-center">
          <td><?=$value['create_date']?></td>
          <td><?=number_format($value['price_declare'],2)?></td>
          <td><?=number_format($value['counting'])?></td>
          <td><?=number_format($value['ddd'],2)?></td>
        </tr>
        <?php } } ?>
      </tbody>
      <tfoot>
        <tr class="text-center">
          <td><strong>รวม</strong></td>
          <td><strong><?=number_format($price_declare,2);?></strong></td>
          <td><strong><?=number_format($counting);?></strong></td>
          <td><strong><?=number_format($ddd,2);?></strong></td>
        </tr>
      </tfoot>
    </table>
  </div>
  <div class="col-md-6 mar-bottom-20">
    <table class="table table-bordered table-striped" style="width:100%;">
      <thead>
        <tr class="text-center">
          <td colspan="4"><strong>CLAIM DECARE VALUE</strong></td>
        </tr>
        <tr class="text-center">
          <td><strong>วันที่แจ้งเคลม</strong></td>
          <td><strong>ค่าเบี้ยประกัน</strong></td>
          <td><strong>จำนวนเคลม</strong></td>
          <td><strong>สินไหมจ่าย</strong></td>
        </tr>
      </thead>
      <tbody>
        <?php
        $sql = "
                  select convert(varchar(10), b.create_date, 103) as create_date,
                  isnull(count(tracking_no),0) as counting,
                  isnull(sum(a.price_declare),0) / 100 as price_declare,
                  isnull(sum(case discount when 0 then (service_dvs/100) else (service_dvs_discount/100) end),0) as ddd
                  from [FLASH_EXPRESS].[dbo].[FLEX_REGIS_DECLARE] a Inner Join
                  [FLASH_EXPRESS].[dbo].[FLEX_CLAIM_DECLARE] b on a.tracking_no = b.tracking_no
                  where replace(convert(varchar(10), b.create_date, 102),'.','-') BETWEEN '$dateStart' and '$dateEnd'
                  and a.claim_status != 'C'
                  group by convert(varchar(10), b.create_date, 103) order by create_date asc
               ";
        $query = DbQuery($sql,null);
        $json  = json_decode($query, true);
        $count = $json['dataCount'];
        $row   = $json['data'];

        $counting = 0;
        $price_declare = 0;
        $ddd = 0;
        if($count > 0){
          foreach ($row as $value) {
            $price_declare += $value['price_declare'];
            $counting += $value['counting'];
            $ddd += $value['ddd'];
        ?>
        <tr class="text-center">
          <td><?=$value['create_date']?></td>
          <td><?=number_format($value['price_declare'],2)?></td>
          <td><?=number_format($value['counting'])?></td>
          <td><?=number_format($value['ddd'],2)?></td>
        </tr>
        <?php } } ?>
      </tbody>
      <tfoot>
        <tr class="text-center">
          <td><strong>รวม</strong></td>
          <td><strong><?=number_format($price_declare,2);?></strong></td>
          <td><strong><?=number_format($counting);?></strong></td>
          <td><strong><?=number_format($ddd,2);?></strong></td>
        </tr>
      </tfoot>
    </table>
  </div>
  <div class="clearfix"></div>
  <div class="col-md-6 mar-bottom-20">
    <table class="table table-bordered table-striped" style="width:100%;">
      <thead>
        <tr class="text-center">
          <td colspan="4"><strong>REGISER DECARE VALUE บัญชี</strong></td>
        </tr>
        <tr class="text-center">
          <td><strong>วันที่</strong></td>
          <td><strong>มูลค่าสิ่งของรับประกัน</strong></td>
          <td><strong>จำนวนกรมธรรม์</strong></td>
          <td><strong>ค่าเบี้ยประกัน</strong></td>
        </tr>
      </thead>
      <tbody>
        <?php
        $sql = "
                select convert(varchar(10), date_receive_product, 103) as create_date,
                isnull(count(tracking_no),0) as counting,
                isnull(sum(price_declare),0) / 100 as price_declare,
                isnull(sum(case discount when 0 then (service_dvs/100) else (service_dvs_discount/100) end),0) as ddd
                from [FLASH_EXPRESS].[dbo].[FLEX_REGIS_DECLARE]
                where replace(convert(varchar(10), date_receive_product, 102),'.','-') BETWEEN '$dateStart' and '$dateEnd'
                and claim_status != 'C'
                group by convert(varchar(10), date_receive_product, 103) order by create_date asc
               ";
        $query = DbQuery($sql,null);
        $json  = json_decode($query, true);
        $count = $json['dataCount'];
        $row   = $json['data'];

        $price_declare = 0;
        $counting = 0;
        $ddd = 0;
        if($count > 0){
          foreach ($row as $value) {
            $price_declare += $value['price_declare'];
            $counting      += $value['counting'];
            $ddd += $value['ddd'];
        ?>
        <tr class="text-center">
          <td><?=$value['create_date']?></td>
          <td><?=number_format($value['price_declare'],2)?></td>
          <td><?=number_format($value['counting'])?></td>
          <td><?=number_format($value['ddd'],2)?></td>
        </tr>
        <?php } } ?>
      </tbody>
      <tfoot>
        <tr class="text-center">
          <td><strong>รวม</strong></td>
          <td><strong><?=number_format($price_declare,2);?></strong></td>
          <td><strong><?=number_format($counting);?></strong></td>
          <td><strong><?=number_format($ddd,2);?></strong></td>
        </tr>
      </tfoot>
    </table>

  </div>
  <div class="col-md-6 mar-bottom-20">
    <table class="table table-bordered table-striped" style="width:100%;">
      <thead>
        <tr class="text-center">
          <td colspan="4"><strong>CLAIM DECARE VALUE บัญชี</strong></td>
        </tr>
        <tr class="text-center">
          <td><strong>วันที่แจ้งเคลม</strong></td>
          <td><strong>ค่าเบี้ยประกัน</strong></td>
          <td><strong>จำนวนเคลม</strong></td>
          <td><strong>สินไหมจ่าย</strong></td>
        </tr>
      </thead>
      <tbody>
        <?php
        $sql = "
                  select convert(varchar(10), b.date_receive_product, 103) as create_date,
                  isnull(count(tracking_no),0) as counting,
                  isnull(sum(a.price_declare),0) / 100 as price_declare,
                  isnull(sum(case discount when 0 then (service_dvs/100) else (service_dvs_discount/100) end),0) as ddd
                  from [FLASH_EXPRESS].[dbo].[FLEX_REGIS_DECLARE] a Inner Join
                  [FLASH_EXPRESS].[dbo].[FLEX_CLAIM_DECLARE] b on a.tracking_no = b.tracking_no
                  where replace(convert(varchar(10), b.date_receive_product, 102),'.','-') BETWEEN '$dateStart' and '$dateEnd'
                  and a.claim_status != 'C'
                  group by convert(varchar(10), b.date_receive_product, 103) order by create_date asc
               ";
        $query = DbQuery($sql,null);
        $json  = json_decode($query, true);
        $count = $json['dataCount'];
        $row   = $json['data'];

        $counting = 0;
        $price_declare = 0;
        $ddd = 0;
        if($count > 0){
          foreach ($row as $value) {
            $price_declare += $value['price_declare'];
            $counting += $value['counting'];
            $ddd += $value['ddd'];
        ?>
        <tr class="text-center">
          <td><?=$value['create_date']?></td>
          <td><?=number_format($value['price_declare'],2)?></td>
          <td><?=number_format($value['counting'])?></td>
          <td><?=number_format($value['ddd'],2)?></td>
        </tr>
        <?php } } ?>
      </tbody>
      <tfoot>
        <tr class="text-center">
          <td><strong>รวม</strong></td>
          <td><strong><?=number_format($price_declare,2);?></strong></td>
          <td><strong><?=number_format($counting);?></strong></td>
          <td><strong><?=number_format($ddd,2);?></strong></td>
        </tr>
      </tfoot>
    </table>
  </div>
  <div class="clearfix"></div>
</div>

<script type="text/javascript">
// $(function () {
//   $(".table").DataTable();
// });
</script>
